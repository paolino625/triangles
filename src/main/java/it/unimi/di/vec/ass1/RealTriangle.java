package it.unimi.di.vec.ass1;

import java.util.Scanner;

public class RealTriangle implements Triangle {

  private final int lato1, lato2, lato3;

  public RealTriangle() {

    Scanner scanner = new Scanner(System.in);

    try {

      // System.out.println("Inserisci la dimensione del primo lato:");
      this.lato1 = Integer.parseInt(scanner.nextLine());

      // System.out.println("Inserisci la dimensione del secondo lato: ");
      this.lato2 = Integer.parseInt(scanner.nextLine());

      // System.out.println("Inserisci la dimensione terzo lato: ");
      this.lato3 = Integer.parseInt(scanner.nextLine());

    } catch (NumberFormatException e) {

      throw new NumberFormatException();
    }

    if (isInvalidTriangle()) {

      throw new IllegalArgumentException();
    }
  }

  private Boolean isInvalidTriangle() {

    if ((lato1 + lato2 <= lato3) || (lato2 + lato3 <= lato1) || (lato1 + lato3 <= lato2)) {

      return Boolean.TRUE;

    } else {

      return Boolean.FALSE;
    }
  }

  @Override
  public void describe() {

    if (lato1 == lato2 && lato2 == lato3) {
      System.out.print("Triangolo equilatero");
    } else if (lato1 == lato2 || lato2 == lato3 || lato1 == lato3) {
      System.out.print("Triangolo isoscele");
    } else {
      System.out.print("Triangolo scaleno");
    }
  }
}
