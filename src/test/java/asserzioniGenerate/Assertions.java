package asserzioniGenerate;

/**
 * Entry point for assertions of different data types. Each method in this class is a static factory
 * for the type-specific assertion objects.
 */
public class Assertions extends org.assertj.core.api.Assertions {

  /**
   * Creates a new instance of <code>{@link RealTriangleAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public static RealTriangleAssert assertThat(it.unimi.di.vec.ass1.RealTriangle actual) {
    return new RealTriangleAssert(actual);
  }

  /**
   * Creates a new instance of <code>{@link TriangleAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public static TriangleAssert assertThat(it.unimi.di.vec.ass1.Triangle actual) {
    return new TriangleAssert(actual);
  }

  protected Assertions() {
    // empty
  }
}
