package vecExtensions_Paolo;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(SystemOutLogExt.class)
@Retention(RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})

// Per salvarsi lo stato non posso utilizzare degli attributi perché JUnit potrebbe distruggere
// l'istanza e crearne un'altra.
// Si potrebbero utilizzare degli attributi statici ma si andrebbe incontro ad altri problemi.
// Si deve accedere a ExtensionStore è una mappa chiave valore con namespace separati a cui abbiamo
// accesso.
// Qui dobbiamo salvarci i valori che dobbiamo rendere comuni per diverse invocazioni di diversi
// metodi di una stessa estensione. Si può anche comunicare con estensioni diverse.

public @interface SystemOutputLog {}
