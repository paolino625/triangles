package vecExtensions_Paolo;

import java.io.ByteArrayInputStream;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class SystemInLogExt implements BeforeEachCallback {

  @Override
  public void beforeEach(ExtensionContext context) throws Exception {

    String sourceString = context.getRequiredTestMethod().getAnnotation(SystemInLog.class).value();
    System.setIn(new ByteArrayInputStream(sourceString.getBytes()));
  }
}
