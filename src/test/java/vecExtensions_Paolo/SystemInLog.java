package vecExtensions_Paolo;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(SystemInLogExt.class)
@Retention(RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface SystemInLog {
  String value();
}
