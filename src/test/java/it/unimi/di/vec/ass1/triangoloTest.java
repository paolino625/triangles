package it.unimi.di.vec.ass1;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

import asserzioniGenerate.Assertions;
import org.junit.jupiter.api.Test;
import vecExtensions_Paolo.SystemInLog;
import vecExtensions_Paolo.SystemOutLogExt;
import vecExtensions_Paolo.SystemOutputLog;

public class triangoloTest {

  @Test
  @SystemOutputLog()
  @SystemInLog("4\n4\n5\n")
  public void testCostruttore_InputCorretto_testLato1(SystemOutLogExt.StdOutLog output) {

    // SETUP + EXERCISE
    RealTriangle triangolo = new RealTriangle();

    // ASSERT

    Assertions.assertThat(triangolo).hasLato1(4);
  }

  @Test
  @SystemOutputLog()
  @SystemInLog("4\n4\n5\n")
  public void testCostruttore_InputCorretto_testLato2(SystemOutLogExt.StdOutLog output) {

    // SETUP + EXERCISE
    RealTriangle triangolo = new RealTriangle();

    // ASSERT

    Assertions.assertThat(triangolo).hasLato2(4);
  }

  @Test
  @SystemOutputLog()
  @SystemInLog("4\n4\n5\n")
  public void testCostruttore_InputCorretto_testLato3(SystemOutLogExt.StdOutLog output) {

    // SETUP + EXERCISE
    RealTriangle triangolo = new RealTriangle();

    // ASSERT

    Assertions.assertThat(triangolo).hasLato3(5);
  }

  @Test
  @SystemOutputLog()
  @SystemInLog("Questi non sono numeri")
  void testCostruttore_InputErrato_Lettere(SystemOutLogExt.StdOutLog output) {

    // SETUP

    // EXERCISE/ASSERT

    try {

      new RealTriangle();
      failBecauseExceptionWasNotThrown(NumberFormatException.class);

    } catch (NumberFormatException e) {

      assertThat(e.getMessage()).isNull();
    }

    // TEARDOWN

    output.reset();
  }

  @Test
  @SystemOutputLog()
  @SystemInLog("2 \n-2 \n4\n")
  void testCostruttore_InputErrato_NumeriNegativi(SystemOutLogExt.StdOutLog output) {

    // SETUP

    // EXERCISE/ASSERT

    try {

      new RealTriangle();
      failBecauseExceptionWasNotThrown(NumberFormatException.class);

    } catch (NumberFormatException e) {

      assertThat(e.getMessage()).isNull();
    }

    // TEARDOWN

    output.reset();
  }

  @Test
  @SystemOutputLog()
  @SystemInLog("\"2 \\n2 \\n4.0\\n")
  void testCostruttore_InputErrato_NumeriFloat(SystemOutLogExt.StdOutLog output) {

    // SETUP

    // EXERCISE/ASSERT

    try {

      new RealTriangle();
      failBecauseExceptionWasNotThrown(NumberFormatException.class);

    } catch (NumberFormatException e) {

      assertThat(e.getMessage()).isNull();
    }

    // TEARDOWN

    output.reset();
  }

  @Test
  @SystemOutputLog()
  @SystemInLog("1\n2\n10\n")
  void testCostruttore_InputErrato_TriangoloNonReale(SystemOutLogExt.StdOutLog output) {

    // SETUP

    // EXERCISE/ASSERT

    try {

      new RealTriangle();
      failBecauseExceptionWasNotThrown(IllegalArgumentException.class);

    } catch (IllegalArgumentException e) {

      assertThat(e.getMessage()).isNull();
    }

    // TEARDOWN

    output.reset();
  }

  @Test
  @SystemOutputLog()
  @SystemInLog("3\n4\n5\n")
  void testDescribe_TriangoloScaleno(SystemOutLogExt.StdOutLog output) {

    // SETUP

    RealTriangle triangolo = new RealTriangle();

    // EXERCISE

    triangolo.describe();

    // ASSERT

    assertThat(output.getStdOutLog()).isEqualTo("Triangolo scaleno");

    // TEARDOWN

    output.reset();
  }

  @Test
  @SystemOutputLog()
  @SystemInLog("4\n4\n5\n")
  void testDescribe_TriangoloIsoscelePrimoSecondoLatiUguali(SystemOutLogExt.StdOutLog output) {

    // SETUP

    RealTriangle triangolo = new RealTriangle();

    // EXERCISE

    triangolo.describe();

    // ASSERT

    assertThat(output.getStdOutLog()).isEqualTo("Triangolo isoscele");

    // TEARDOWN

    output.reset();
  }

  @Test
  @SystemOutputLog()
  @SystemInLog("3\n4\n3\n")
  void testDescribe_TriangoloIsosceleSecondoTerzoLatiUguali(SystemOutLogExt.StdOutLog output) {

    // SETUP

    RealTriangle triangolo = new RealTriangle();

    // EXERCISE

    triangolo.describe();

    // ASSERT

    assertThat(output.getStdOutLog()).isEqualTo("Triangolo isoscele");

    // TEARDOWN

    output.reset();
  }

  @Test
  @SystemOutputLog()
  @SystemInLog("5\n4\n5\n")
  void testDescribe_TriangoloIsoscelePrimoTerzoLatiUguali(SystemOutLogExt.StdOutLog output) {

    // SETUP

    RealTriangle triangolo = new RealTriangle();

    // EXERCISE

    triangolo.describe();

    // ASSERT

    assertThat(output.getStdOutLog()).isEqualTo("Triangolo isoscele");

    // TEARDOWN

    output.reset();
  }

  @Test
  @SystemOutputLog()
  @SystemInLog("4\n4\n4\n")
  void testDescribe_TriangoloEquilatero(SystemOutLogExt.StdOutLog output) {

    // SETUP

    RealTriangle triangolo = new RealTriangle();

    // EXERCISE

    triangolo.describe();

    // ASSERT

    assertThat(output.getStdOutLog()).isEqualTo("Triangolo equilatero");

    // TEARDOWN

    output.reset();
  }
}
